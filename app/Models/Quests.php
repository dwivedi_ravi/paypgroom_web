<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Quests extends Model
{
    // protected $fillable = ['pg_booking_id'];
    protected $fillable = ['pg_booking_id','quest_name','quest_mobile','quest_document_type','quest_document_number'];
}
