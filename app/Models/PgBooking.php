<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PgBooking extends Model
{   
    protected $fillable = [
    
    'payment_id',
    'tenant_id' ,
                        'pg_id' ,
                        'owner_id',
                        'room_type_id',
                        'from_date',
                        'to_date',
                        'booking_price',
                        'security_deposit',
                        'cleaning_fee',
                        'coupan_code',
                        'discount',
                        'total_amount',
                        'total_amount_after_discount',
                        'number_of_months',
                        'payment_mode',
                        'merchant_id',
                        'merchant_key',
                        'transection_history_id',
                        'status',
                        'booking_status_text'];
    
    use SoftDeletes;
    function pgDetail() {
        return $this->hasOne('App\Models\Pgdetail', 'id', 'pg_id')->withTrashed();
    }

    function tenantDetail() {
        return $this->hasOne('App\Models\User', 'id', 'tenant_id');
    }
}
