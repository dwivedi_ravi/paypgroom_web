<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TransectionHistory extends Model
{
    protected $fillable = ['transection_id','tenant_id','pg_id','owner_id','hash_token'];
    use SoftDeletes;
}
